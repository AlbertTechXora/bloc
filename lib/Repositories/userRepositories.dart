import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/services.dart';

class UserRepository {
  late FirebaseAuth firebaseAuth;

  UserRepository() {
    this.firebaseAuth = FirebaseAuth.instance;
  }

  Future<User?> createUser(String email, String password) async {
    try {
      var res = await firebaseAuth.createUserWithEmailAndPassword(
          email: email, password: password);
      return res.user;
    } on PlatformException catch (e) {
      throw Exception(e.toString());
    }
  }

  Future<User?> signIn(String email, String password) async {
    var res = await firebaseAuth.createUserWithEmailAndPassword(
        email: email, password: password);
    return res.user;
  }

  Future signOut() async {
    await firebaseAuth.signOut();
  }

  Future<bool> isSignedIn() async {
    var currUser = firebaseAuth.currentUser;
    return currUser != null;
  }

  Future<User?> getCurrentUser() async {
    final User? getCurrUser = firebaseAuth.currentUser;
    return getCurrUser;
  }

  getUser() {}
}
