import 'dart:async';

mixin Validators {
  var emailValidator = StreamTransformer<String, String>.fromHandlers(
      handleData: (emailStream, sink) {
    if (emailStream.contains("@")) {
      sink.add(emailStream);
    } else {
      sink.addError("Email is not Valid");
    }
  });
  var pswdValidator = StreamTransformer<String, String>.fromHandlers(
      handleData: (pswdStream, sink) {
    if (pswdStream.length < 6) {
      sink.add(pswdStream);
    } else {
      sink.addError("Password length should be atlast 6");
    }
  });
}
