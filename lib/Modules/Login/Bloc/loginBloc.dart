import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_bloc_app/Modules/Home/homeScreen.dart';
import 'package:rxdart/rxdart.dart';

import 'package:flutter_bloc_app/Modules/Login/Bloc/validator.dart';

class LoginBloc extends Object with Validators implements BaseBloc {
  final _emailController = BehaviorSubject<String>();
  final _pswdController = BehaviorSubject<String>();

  Function(String) get emailChanged => _emailController.sink.add;
  Function(String) get pswdChanged => _pswdController.sink.add;

  Stream<String> get emailStream =>
      _emailController.stream.transform(emailValidator);
  Stream<String> get pswdStream =>
      _pswdController.stream.transform(pswdValidator);
  Stream<bool> get submitCheck =>
      Rx.combineLatest2(emailStream, pswdStream, (email, pswd) => true);

  submit() {
    print("LoggedIn");
  }

  static Route route() {
    return MaterialPageRoute<void>(builder: (_) => HomeScreen());
  }

  @override
  void dispose() {
    _emailController.close();
    _pswdController.close();
  }
}

abstract class BaseBloc {
  void dispose();
}
