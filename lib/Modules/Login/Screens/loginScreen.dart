import 'package:flutter/material.dart';
import 'package:flutter_bloc_app/Modules/Home/homeScreen.dart';
import 'package:flutter_bloc_app/Modules/Login/Bloc/loginBloc.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  submitData() {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => HomeScreen()),
        (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    final loginBloc = LoginBloc();
    return Scaffold(
      appBar: AppBar(
        title: Text("Login BLoC"),
      ),
      body: SafeArea(
        child: Container(
          margin: EdgeInsets.fromLTRB(16, 60, 16, 0),
          child: SingleChildScrollView(
            child: Column(
              children: [
                Text(
                  "BLoC Login",
                  style: TextStyle(fontSize: 50.0),
                ),
                SizedBox(
                  height: 20.0,
                ),
                StreamBuilder<String>(
                  stream: loginBloc.emailStream,
                  builder: (context, snapshot) {
                    return TextField(
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        labelText: "Email",
                        prefixIcon: Icon(Icons.email),
                      ),
                      keyboardType: TextInputType.emailAddress,
                      onChanged: loginBloc.emailChanged,
                    );
                  },
                ),
                SizedBox(
                  height: 20.0,
                ),
                StreamBuilder<String>(
                  stream: loginBloc.pswdStream,
                  builder: (context, snapshot) {
                    return TextField(
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        labelText: "Password",
                        prefixIcon: Icon(Icons.lock),
                      ),
                      keyboardType: TextInputType.visiblePassword,
                      obscureText: true,
                      onChanged: loginBloc.pswdChanged,
                    );
                  },
                ),
                SizedBox(
                  height: 20.0,
                ),
                StreamBuilder<bool>(
                  stream: loginBloc.submitCheck,
                  builder: (context, snapshot) {
                    print(snapshot.hasData);
                    return ElevatedButton(
                      onPressed: snapshot.hasData ? submitData : null,
                      child: Text(
                        "Login",
                        style: TextStyle(fontSize: 14.0, color: Colors.white),
                      ),
                      style: ButtonStyle(
                        shape: MaterialStateProperty.all(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20),
                          ),
                        ),
                        backgroundColor: MaterialStateProperty.all(Colors.teal),
                        padding: MaterialStateProperty.all(
                          EdgeInsets.all(14),
                        ),
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
