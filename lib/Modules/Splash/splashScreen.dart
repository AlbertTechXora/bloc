import 'package:flutter/material.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);

  static Route route() {
    return MaterialPageRoute<void>(builder: (_) => SplashScreen());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("BLoC Splash"),
      ),
      body: Container(
        child: Center(
          child: Text(
            "Splash Screen",
            style: TextStyle(
              color: Colors.tealAccent,
              fontSize: 30.0,
            ),
          ),
        ),
      ),
    );
  }
}
