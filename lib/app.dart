import 'package:flutter/material.dart';
import 'package:flutter_bloc_app/Modules/Post/postPage.dart';

class App extends MaterialApp {
  App() : super(home: PostsPage());
}